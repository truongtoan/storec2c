﻿namespace SGUStoreService.Data.Models
{
    public enum Gender
    {
        Man,
        Woman,
        Other
    }
}